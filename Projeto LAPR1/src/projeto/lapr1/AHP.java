/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.lapr1;

import java.io.FileNotFoundException;
import org.la4j.Matrix;
import org.la4j.decomposition.EigenDecompositor;
import org.la4j.matrix.dense.Basic2DMatrix;

/**
 *
 * @author Grupo 4
 */
public class AHP {

    /**
     * guarda as matrizes em memória
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param linha guarda o conteúdo da linha não-vazia atual
     * @param util guarda o estado atual de leitura do ficheiro
     * @param cabcomp guarda o cabeçalho das matrizes de comparação
     * @param crit guarda a matriz dos critérios
     * @param comp guarda as matrizes de comparação
     * @return
     * @throws FileNotFoundException
     */
    public static int[] guardarMatrizes(int N_CRITERIOS, int N_ALTERNATIVAS, String linha, int[] util, String[][] cabcomp, double[][] crit, double[][][] comp) throws FileNotFoundException {
        String temp = linha.replaceAll(" +", " ");  //apaga os espaços múltiplos da variável linha e atribui o resultado à variável temp
        String[] valor = temp.split(" ");   //separa o conteúdo da variável temp com auxílio do delimitador " " e atribui o resultado ao vetor de strings valor
        String[] quo;
        int i;
        if (util[0] == 1) { //se for a 1ª matriz
            if (!linha.contains("mc")) { //e se não estivermos no cabeçalho
                for (i = 0; i < N_CRITERIOS; i++) { //lê a matriz propriamente dita
                    if (valor[i].contains("/")) {   //se o elemento do vetor possui / (fração)
                        quo = valor[i].split("/");  //divide o elemento pelo delimitador "/" e atribui à variável quo
                        crit[util[1]][i] = Double.parseDouble(quo[0]) / Double.parseDouble(quo[1]); //converte ambos os elementos em double e efetua o cálculo
                    } else {    //senão
                        crit[util[1]][i] = Double.parseDouble(valor[i]);    //converte simplesmente a string em double
                    }
                }
                util[1]++;  //incrementa o nº que representa linha da matriz atual
                if (util[1] > N_CRITERIOS - 1) {    //se estivermos na última linha da matriz
                    util[0]++;  //incrementa o nº que representa a matriz atual
                    util[1] = 0;    //reseta o nº da linha da matriz atual
                }
            }
        } else {    //senão faz semelhante processo para as matrizes das alternativas
            if (linha.contains("mc")) {
                cabcomp[util[0] - 2] = valor;
            } else {
                for (i = 0; i < N_ALTERNATIVAS; i++) {
                    if (valor[i].contains("/")) {
                        quo = valor[i].split("/");
                        comp[util[0] - 2][util[1]][i] = Double.parseDouble(quo[0]) / Double.parseDouble(quo[1]);
                    } else {
                        comp[util[0] - 2][util[1]][i] = Double.parseDouble(valor[i]);
                    }
                }
                util[1]++;
                if (util[1] > N_ALTERNATIVAS - 1) {
                    util[0]++;
                    util[1] = 0;
                }
            }
        }
        return util;    //retorna util que permite saber o ponto de situação da leitura e armazenamento dos dados do ficheiro em memória
    }

    /**
     * permite copiar matrizes
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param crit guarda a matriz dos critérios que queremos copiar
     * @param copiacrit recebe a matriz dos critérios que queremos copiar
     * @param comp guarda as matrizes de comparação que queremos copiar
     * @param copiacomp recebe as matrizes de comparação que queremos copiar
     */
    public static void copiarMatrizes(int N_ALTERNATIVAS, double[][] crit, double[][] copiacrit, double[][][] comp, double[][][] copiacomp) {
        int i, j, k;
        for (i = 0; i < crit.length; i++) {
            for (j = 0; j < crit.length; j++) {
                copiacrit[i][j] = crit[i][j];
            }
            for (j = 0; j < N_ALTERNATIVAS; j++) {
                for (k = 0; k < N_ALTERNATIVAS; k++) {
                    copiacomp[i][j][k] = comp[i][j][k];
                }
            }
        }
    }

    /**
     * permite remover critérios que estejam abaixo do limiar dos pesos recebido
     * como argumento da linha de comandos
     *
     * @param limpeso limiar do peso recebido como argumento
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     * @param copiacrit guarda a matriz dos critérios
     * @param copiacomp guarda as matrizes de comparação
     * @return
     */
    public static int limparMatrizes(double limpeso, int N_CRITERIOS, int N_ALTERNATIVAS, double[] vetcrit, double[][] copiacrit, double[][][] copiacomp) {
        int i, j, k;
        for (i = 0; i < copiacrit.length; i++) {
            if (vetcrit[i] < limpeso) {
                for (j = 0; j < copiacrit.length; j++) {
                    copiacrit[i][j] = 0;
                    copiacrit[j][i] = 0;
                }
                for (j = 0; j < N_ALTERNATIVAS; j++) {
                    for (k = 0; k < N_ALTERNATIVAS; k++) {
                        copiacomp[i][j][k] = 0;
                    }
                }
                N_CRITERIOS--;
            }
        }
        return N_CRITERIOS;
    }

    /**
     * normaliza as matrizes
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param crit guarda a matriz dos critérios normalizada
     * @param comp guarda as matrizes de comparação normalizadas
     */
    public static void normalizarMatrizes(int N_ALTERNATIVAS, double[][] crit, double[][][] comp) {
        int i, j, k;
        double somac = 0;   //armazena o resultado da soma dos elementos da coluna atual
        boolean conv = false;   //permite saber se a normalização já foi ou não feita
        for (k = 0; k < crit.length; k++) {
            for (j = 0; j < N_ALTERNATIVAS; j++) {
                for (i = 0; i < N_ALTERNATIVAS; i++) {
                    if (!conv) {    //se a normalização não foi feita
                        somac = somac + comp[k][i][j];  //efetua a soma da coluna atual

                    } else {    //senão
                        if (somac != 0) {
                            comp[k][i][j] = comp[k][i][j] / somac;  //efetua a normalização
                        } else {
                            comp[k][i][j] = 0;
                        }
                    }
                }
                if (!conv) {    //se não foi feita a normalização
                    j--;    //diminui em 1 unidade a coluna atual
                    conv = true;    //define a normalização como feita (na verdade só irá ser feita na próxima iteração do ciclo
                } else {    //se foi feita
                    somac = 0;  //reseta a soma da coluna atual
                    conv = false;   //define a normalização por fazer
                }
            }
        }
        for (j = 0; j < crit.length; j++) { //processo semelhante para a matriz dos critérios
            for (i = 0; i < crit.length; i++) {
                if (!conv) {
                    somac = somac + crit[i][j];
                } else {
                    if (somac != 0) {
                        crit[i][j] = crit[i][j] / somac;
                    } else {
                        crit[i][j] = 0;
                    }
                }
            }
            if (!conv) {
                j--;
                conv = true;
            } else {
                somac = 0;
                conv = false;
            }
        }
    }

    /**
     * calcula os vetores próprios (aproximado)
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param crit guarda a matriz dos critérios normalizada
     * @param comp guarda as matrizes de comparação normalizadas
     * @param vetcomp guarda os vetores próprios associados às matrizes de
     * comparação
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     */
    public static void vetorProprio(int N_CRITERIOS, int N_ALTERNATIVAS, double[][] crit, double[][][] comp, double[][] vetcomp, double[] vetcrit) {
        int i, j, k;
        double soma = 0;    //faz a soma da linha atual da matriz
        for (k = 0; k < crit.length; k++) {
            for (j = 0; j < N_ALTERNATIVAS; j++) {
                for (i = 0; i < N_ALTERNATIVAS; i++) {
                    soma = soma + comp[k][j][i];    //soma a linha atual da matriz
                }
                vetcomp[k][j] = soma / N_ALTERNATIVAS;  //faz o cálculo do vetor próprio associado a cada uma das matrizes de alternativas
                soma = 0;   //reseta a soma da linha atual da matriz
            }
        }
        for (j = 0; j < crit.length; j++) { //processo semelhante para a matriz dos critérios
            for (i = 0; i < crit.length; i++) {
                soma = soma + crit[j][i];
            }
            vetcrit[j] = soma / N_CRITERIOS;
            soma = 0;
        }
    }

    /**
     * calcula os maiores valores próprios (aproximado)
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param copiacrit guarda a matriz dos critérios
     * @param copiacomp guarda as matrizes de comparação
     * @param vetcomp guarda os vetores próprios associados às matrizes de
     * comparação
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     * @param valcomp guarda os maiores valores próprios associados às matrizes
     * de comparação
     * @param valcrit guarda o maior valor próprio associado à matriz dos
     * critérios
     * @return
     */
    public static double valorProprio(int N_CRITERIOS, int N_ALTERNATIVAS, double[][] copiacrit, double[][][] copiacomp, double[][] vetcomp, double[] vetcrit, double[] valcomp, double valcrit) {
        int i, j, k;
        double soma = 0, aux, somaaux = 0;  //soma armazena o produto do vetor próprio associado pela matriz não normalizada, aux o quociente entre a soma e o vetor próprio e somaaux o somatório dos diversos aux
        for (k = 0; k < copiacrit.length; k++) {
            for (j = 0; j < N_ALTERNATIVAS; j++) {
                for (i = 0; i < N_ALTERNATIVAS; i++) {
                    soma = soma + (vetcomp[k][i] * copiacomp[k][j][i]); //calcula o produto da matriz não normalizada pelo vetor próprio associado
                }
                aux = soma / vetcomp[k][j]; //divide a soma pelo vetor próprio
                somaaux = somaaux + aux;    //soma os diversos aux
                soma = 0;   //reseta a soma
            }
            valcomp[k] = somaaux / N_ALTERNATIVAS;  //calcula o máximo valor próprio associado a cada uma das matrizes de alternativas
            somaaux = 0;    //reseta o somaaux
        }
        for (j = 0; j < copiacrit.length; j++) { //processo semelhante para a matriz dos critérios
            for (i = 0; i < copiacrit.length; i++) {
                soma = soma + (vetcrit[i] * copiacrit[j][i]);
            }
            if (vetcrit[j] != 0) {
                aux = soma / vetcrit[j];
            } else {
                aux = 0;
            }
            somaaux = somaaux + aux;
            soma = 0;
        }
        valcrit = somaaux / N_CRITERIOS;
        return valcrit;
    }

    /**
     * calcula os vetores e valores próprios (exato)
     *
     * @param copiacrit guarda a matriz dos critérios
     * @param copiacomp guarda as matrizes de comparação
     * @param vetcomp guarda os vetores próprios associados às matrizes de
     * comparação
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     * @param valcomp guarda os maiores valores próprios associados às matrizes
     * de comparação
     * @param valcrit guarda o maior valor próprio associado à matriz dos
     * critérios
     * @return
     */
    public static double valoresExatos(double[][] copiacrit, double[][][] copiacomp, double[][] vetcomp, double[] vetcrit, double[] valcomp, double valcrit) {
        int i, j, k, ind = 0;   //ind armazena a coluna que contém o máximo valor próprio no objeto Matrix criado para esse efeito
        double maior = 0, soma = 0; //maior armazena o maior valor próprio, enquanto que soma serve para efeitos de normalização dos vetores próprios associados
        boolean flag = false;   //verifica se a normalização dos vetores foi ou não feita
        Matrix auxcrit = new Basic2DMatrix(copiacrit);  //cria um objeto do tipo Matrix
        EigenDecompositor eigencrit = new EigenDecompositor(auxcrit);   //cria um objeto do tipo EigenDecompositor com base na Matrix criada na linha anterior
        Matrix[] auxeigencrit = eigencrit.decompose();  //"decompõe" o EigenDecompositor, ou seja, divide num vetor de Matrix para cálculo de valores e vetores próprios
        double auxvetcrit[][] = auxeigencrit[0].toDenseMatrix().toArray();  //converte a Matrix na posição 0 do vetor anterior para uma matriz "normal" do tipo double, com os vetores próprios associados à matriz
        double auxvalcrit[][] = auxeigencrit[1].toDenseMatrix().toArray();  //converte a Matrix na posição 1 do vetor anterior para uma matriz "normal" do tipo double, com os valores próprios associados à matriz
        for (i = 0; i < auxvalcrit.length; i++) {   //ciclo dos valores próprios da matriz atual
            for (j = 0; j < auxvalcrit[i].length; j++) {
                if (auxvalcrit[j][i] > maior) { //se o valor próprio nessa posição for maior que o conteúdo da variável maior
                    maior = auxvalcrit[j][i];   //atribui à variável maior o valor próprio nessa posição
                    ind = j;    //armazena a posição da coluna com o atual maior valor próprio
                }
            }
        }
        valcrit = maior;    //o maior valor próprio, representado pela variável maior, é finalmente atribuido à variável global designada para o armazenar
        for (i = 0; i < auxvetcrit.length; i++) {   //ciclo dos vetores próprios da matriz atual
            if (auxvetcrit[i][ind] < 0) {   //se o elemento do vetor próprio associado ao maior valor próprio for negativo
                auxvetcrit[i][ind] = auxvetcrit[i][ind] * -1;   //converter para positivo
            }
            if (!flag) {    //caso não tenha sido feita a normalização
                soma = soma + auxvetcrit[i][ind];   //efetuar a soma da coluna
                if (i == auxvetcrit.length - 1) {   //caso estejamos na última posição do vetor
                    flag = true;    //definir a normalização como feita (na verdade ela só irá ser feita na próxima iteração do ciclo)
                    i = -1; //reset à sentinela (-1 porque o ciclo iria incrementá-la brevemente)
                }
            } else {    //senão
                vetcrit[i] = auxvetcrit[i][ind] / soma; //efetuar a normalização
            }
        }
        ind = 0;    //reseta à coluna com maior valor próprio associado
        maior = 0;  //reseta ao maior valor próprio
        soma = 0;   //reset à soma da coluna
        flag = false;   //definir a normalização por fazer
        for (k = 0; k < copiacrit.length; k++) { //processo semelhante para as matrizes de alternativas
            Matrix auxcomp = new Basic2DMatrix(copiacomp[k]);
            EigenDecompositor eigencomp = new EigenDecompositor(auxcomp);
            Matrix[] auxeigencomp = eigencomp.decompose();
            double auxvetcomp[][] = auxeigencomp[0].toDenseMatrix().toArray();
            double auxvalcomp[][] = auxeigencomp[1].toDenseMatrix().toArray();
            for (i = 0; i < auxvalcomp.length; i++) {
                for (j = 0; j < auxvalcomp[i].length; j++) {
                    if (auxvalcomp[j][i] > maior) {
                        maior = auxvalcomp[j][i];
                        ind = j;
                    }
                }
            }
            valcomp[k] = maior;
            for (i = 0; i < auxvetcomp.length; i++) {
                if (auxvetcomp[i][ind] < 0) {
                    auxvetcomp[i][ind] = auxvetcomp[i][ind] * -1;
                }
                if (!flag) {
                    soma = soma + auxvetcomp[i][ind];
                    if (i == auxvetcomp.length - 1) {
                        flag = true;
                        i = -1;
                    }
                } else {
                    vetcomp[k][i] = auxvetcomp[i][ind] / soma;
                }
            }
            ind = 0;
            maior = 0;
            soma = 0;
            flag = false;
        }
        return valcrit;
    }

    /**
     * calcula os indíces de consistência das matrizes
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param valcomp guarda os maiores valores próprios associados às matrizes
     * de comparação
     * @param indcomp guarda os índices de consistência associados às matrizes
     * de comparação
     * @param valcrit guarda o maior valor próprio associado à matriz dos
     * critérios
     * @param indcrit guarda o índice de consistência associado à matriz dos
     * critérios
     * @return
     */
    public static double indiceConsistencia(int N_CRITERIOS, int N_ALTERNATIVAS, double[] valcomp, double[] indcomp, double valcrit, double indcrit) {
        int k;
        for (k = 0; k < valcomp.length; k++) {
            indcomp[k] = (valcomp[k] - N_ALTERNATIVAS) / (N_ALTERNATIVAS - 1);  //calcula o IC da matriz atual
        }
        indcrit = (valcrit - N_CRITERIOS) / (N_CRITERIOS - 1);  //processo semelhante para a matriz dos critérios
        return indcrit;
    }

    /**
     * calcula as razões de consistência das matrizes
     *
     * @param valtab vetor que alberga os valores tabelados para cálculo das
     * razões
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param razcomp guarda as razões de consistência associadas às matrizes de
     * comparação
     * @param indcomp guarda os índices de consistência associados às matrizes
     * de comparação
     * @param indcrit guarda o índice de consistência associado à matriz dos
     * critérios
     * @param razcrit guarda a razão de consistência associada à matriz dos
     * critérios
     * @return
     */
    public static double razaoConsistencia(double[] valtab, int N_CRITERIOS, int N_ALTERNATIVAS, double[] razcomp, double[] indcomp, double indcrit, double razcrit) {
        int k;
        for (k = 0; k < razcomp.length; k++) {
            razcomp[k] = indcomp[k] / valtab[N_ALTERNATIVAS - 1];   //calcula a RC da matriz atual
        }
        if (valtab[N_CRITERIOS - 1] != 0) {
            razcrit = indcrit / valtab[N_CRITERIOS - 1];    //processo semelhante para a matriz dos critérios
        } else {
            razcrit = 0;
        }
        return razcrit;   //return da variável verif, que determina se TODAS as matrizes são consistentes ou se há alguma em falha
    }

    /**
     * calcula o vetor de prioridade composta
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param vetalt guarda o vetor de prioridade composta
     * @param vetcomp guarda os vetores próprios associados às matrizes de
     * comparação
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     */
    public static void vetorAlternativa(int N_ALTERNATIVAS, double[] vetalt, double[][] vetcomp, double[] vetcrit) {
        int j, k;
        double soma = 0;    //calcula o produto dos vetores próprios associados às matrizes das alternativas com os pesos dos critérios
        for (k = 0; k < N_ALTERNATIVAS; k++) {
            for (j = 0; j < vetcrit.length; j++) {
                soma = soma + (vetcomp[j][k] * vetcrit[j]); //cálculo descrito em cima
            }
            vetalt[k] = soma;   //atribui o resultado final ao vetor de prioridade composta para o critério atual
            soma = 0;   //reseta a soma
        }
    }
}
