/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.Scanner;

/**
 *
 * @author Grupo 4
 */
public class LeituraEscrita {

    /**
     * permite saber o nº de critérios e alternativas
     *
     * @param nomefich guarda o nome do ficheiro a ler
     * @param metodo guarda o método a utilizar nos cálculos
     * @param util guarda o estado atual de leitura do ficheiro
     * @return
     * @throws FileNotFoundException
     */
    public static int[] auxLerFicheiro(String nomefich, int metodo, int[] util) throws FileNotFoundException {
        String temp;    //apaga os espaços múltiplos da variável linha e atribui o resultado à variável temp
        String[] valor; //separa o conteúdo da variável temp com auxílio do delimitador " " e atribui o resultado ao vetor de strings valor
        int aux = 1, i;    //verifica se estamos a ler os critérios ou as alternativas
        Scanner ler = new Scanner(new File(nomefich));  //definir o scanner para ler o ficheiro
        while (ler.hasNext()) { //enquanto existir linhas a ler no ficheiro
            String linha = ler.nextLine();  //atribuir a linha seguinte à variável linha
            if (linha.length() > 0 && metodo == 1) {   //se a linha não estiver vazia e for utilizado o método AHP
                temp = linha.replaceAll(" +", " ");  //apaga os espaços múltiplos da variável linha e atribui o resultado à variável temp
                valor = temp.split(" ");   //separa o conteúdo da variável temp com auxílio do delimitador " " e atribui o resultado ao vetor de strings valor
                if (linha.contains("mc") && aux == 2) { //se estivermos no cabeçalho da 1ª matriz de alternativas
                    util[3] = valor.length - 1; //atribui ao vetor util na posição 3 o nº de alternativas
                    aux++;  //incrementa a variável aux
                }
                if (linha.contains("mc") && aux == 1) { //se estivermos no cabeçalho da matriz dos critérios
                    util[2] = valor.length - 1; //atribui ao vetor util na posição 2 o nº de critérios
                    aux++;  //incrementa a variável aux
                }

            }
            if (linha.length() > 0 && metodo == 2) {   //se a linha não estiver vazia e for utilizado o método TOPSIS
                temp = linha.replaceAll(" +", " ");  //apaga os espaços múltiplos da variável linha e atribui o resultado à variável temp
                valor = temp.split(" ");   //separa o conteúdo da variável temp com auxílio do delimitador " " e atribui o resultado ao vetor de strings valor
                if (valor[0].compareToIgnoreCase("crt") == 0) { //se a linha contém os critérios
                    util[2] = valor.length - 1;
                }
                if (valor[0].compareToIgnoreCase("alt") == 0) { //se a linha contém os critérios
                    util[3] = valor.length - 1;
                }
            }
        }
        return util;    //retorna util, para verificar se o ficheiro contém pelo menos duas matrizes
    }

    /**
     * lê o ficheiro e guarda as matrizes em memória, consoante o método
     *
     * @param ficheiroErros guarda o nome do ficheiro de erros
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param nomefich guarda o nome do ficheiro a ler
     * @param metodo guarda o método a utlizar nos cálculos
     * @param util guarda o estado atual da leitura do ficheiro
     * @param beneficio guarda os critérios beneficio (apenas necessário no
     * TOPSIS)
     * @param custo guarda os critérios custo (apenas necessário no TOPSIS)
     * @param pesos guarda os pesos dos critérios (apenas necessário no TOPSIS)
     * @param criterios guarda todos os critérios (apenas necessário no TOPSIS)
     * @param alternativas guarda as alternativas (apenas necessário no TOPSIS)
     * @param matriz guarda a matriz critérios/alternativas (apenas necessário
     * no TOPSIS)
     * @param cabcomp guarda o cabeçalho das matrizes de comparação (apenas
     * necessário no AHP)
     * @param crit guarda a matriz dos critérios (apenas necessário no AHP)
     * @param comp guarda as matrizes de comparação (apenas necessário no AHP)
     * @return
     * @throws FileNotFoundException
     */
    public static int[] lerFicheiro(String ficheiroErros, int N_CRITERIOS, int N_ALTERNATIVAS, String nomefich, int metodo, int[] util, String[] beneficio, String[] custo, double[] pesos, String[] criterios, String[] alternativas, double[][] matriz, String[][] cabcomp, double[][] crit, double[][][] comp) throws FileNotFoundException {
        Scanner ler = new Scanner(new File(nomefich));  //definir o scanner para ler o ficheiro
        if (metodo == 1) {
            util[0] = 1;
            util[1] = 0;
        }
        while (ler.hasNext()) { //enquanto existir linhas a ler no ficheiro
            String linha = ler.nextLine();  //atribuir a linha seguinte à variável linha
            if (linha.length() > 0 && metodo == 1) {   //se a linha não estiver vazia e for utilizado o método AHP
                util = AHP.guardarMatrizes(N_CRITERIOS, N_ALTERNATIVAS, linha, util, cabcomp, crit, comp);    //guardar as matrizes em memória
            }
            if (linha.length() > 0 && metodo == 2) {   //se a linha não estiver vazia e for utilizado o método TOPSIS
                util = TOPSIS.guardarTopsis(ficheiroErros, N_CRITERIOS, linha, util, beneficio, custo, pesos, criterios, alternativas, matriz);    //guardar as matrizes em memória
            }
        }
        return util;    //retorna util, para verificar se o ficheiro contém pelo menos duas matrizes
    }

    /**
     * trata dos outputs
     *
     * @param proximidade guarda a proximidade relativa à solução ideal (apenas
     * necessário no TOPSIS)
     * @param disneg guarda a distância à solução ideal negativa (apenas
     * necessário no TOPSIS)
     * @param disideal guarda a distância à solução ideal (apenas necessário no
     * TOPSIS)
     * @param solneg guarda a solução ideal negativa (apenas necessário no
     * TOPSIS)
     * @param solideal guarda a solução ideal (apenas necessário no TOPSIS)
     * @param pesada guarda a matriz critérios/alternativas normalizada pesada
     * (apenas necessário no TOPSIS)
     * @param normalizada guarda a matriz critérios/alternativas normalizada
     * (apenas necessário no TOPSIS)
     * @param matriz guarda a matriz critérios/alternativas (apenas necessário
     * no TOPSIS)
     * @param alternativas guarda as alternativas (apenas necessário no TOPSIS)
     * @param criterios guarda os critérios (apenas necessário no TOPSIS)
     * @param pesos guarda os pesos dos critérios (apenas necessário no TOPSIS)
     * @param metodo guarda o método a utlizar nos cálculos
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param fich_out guarda o nome do ficheiro da saída
     * @param cabcomp guarda o cabeçalho das matrizes de comparação (apenas
     * necessário no AHP)
     * @param vetalt guarda o vetor de prioridade composta (apenas necessário no
     * AHP)
     * @param razcomp guarda as razões de consistência associadas às matrizes de
     * comparação (apenas necessário no AHP)
     * @param crit guarda a matriz dos critérios normalizada (apenas necessário
     * no AHP)
     * @param copiacrit guarda a matriz dos critérios (apenas necessário no AHP)
     * @param comp guarda as matrizes de comparação normalizadas (apenas
     * necessário no AHP)
     * @param copiacomp guarda as matrizes de comparação (apenas necessário no
     * AHP)
     * @param vetcomp guarda os vetores próprios associados às matrizes de
     * comparação (apenas necessário no AHP)
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     * (apenas necessário no AHP)
     * @param valcomp guarda os maiores valores próprios associados às matrizes
     * de comparação (apenas necessário no AHP)
     * @param indcomp guarda os índices de consistência associados às matrizes
     * de comparação (apenas necessário no AHP)
     * @param valcrit guarda o maior valor próprio associado à matriz dos
     * critérios (apenas necessário no AHP)
     * @param indcrit guarda o índice de consistência associado à matriz dos
     * critérios (apenas necessário no AHP)
     * @param razcrit guarda a razão de consistência associada à matriz dos
     * critérios (apenas necessário no AHP)
     * @throws FileNotFoundException
     */
    public static void outputs(double[] proximidade, double[] disneg, double[] disideal, double[] solneg, double[] solideal, double[][] pesada, double[][] normalizada, double[][] matriz, String[] alternativas, String[] criterios, double[] pesos, int metodo, int N_CRITERIOS, int N_ALTERNATIVAS, String fich_out, String[][] cabcomp, double[] vetalt, double[] razcomp, double[][] crit, double[][] copiacrit, double[][][] comp, double[][][] copiacomp, double[][] vetcomp, double[] vetcrit, double[] valcomp, double[] indcomp, double valcrit, double indcrit, double razcrit) throws FileNotFoundException {
        Formatter escrever = new Formatter(new File(fich_out));   //define o formatter para escrever no ficheiro de saída
        if (metodo == 1) {
            matrizesEntrada(N_ALTERNATIVAS, escrever, cabcomp, copiacrit, copiacomp);  //imprime as matrizes de entrada
            matrizesNormalizadas(N_ALTERNATIVAS, escrever, cabcomp, crit, comp, vetcomp, vetcrit); //imprime as matrizes normalizadas
            valores(crit, escrever, cabcomp, razcomp, valcomp, indcomp, valcrit, indcrit, razcrit);  //imprime o máximo valor próprio, o IC e a RC de cada matriz
            prioridadeComposta(N_ALTERNATIVAS, escrever, cabcomp, vetalt);   //imprime  o vetor de prioridade composta
            alternativaEscolhida(metodo, N_ALTERNATIVAS, escrever, cabcomp[0], vetalt); //calcula e apresenta a melhor alternativa
            criteriosExcluidos(escrever, cabcomp, copiacrit);
            escrever.format("%n%s", "Foi utilizado o metodo AHP.");
            System.out.printf("%n%s", "Foi utilizado o metodo AHP.");
        }
        if (metodo == 2) {
            vetorPesos(criterios, N_CRITERIOS, pesos, escrever);
            matrizDecisao(alternativas, criterios, N_ALTERNATIVAS, N_CRITERIOS, matriz, escrever);
            matrizNormalizada(alternativas, criterios, N_ALTERNATIVAS, N_CRITERIOS, normalizada, escrever);
            matrizPesada(alternativas, criterios, N_ALTERNATIVAS, N_CRITERIOS, pesada, escrever);
            solucaoIdeal(criterios, N_CRITERIOS, solideal, escrever);
            solucaoNegativa(criterios, N_CRITERIOS, solneg, escrever);
            distancias(alternativas, N_ALTERNATIVAS, disideal, disneg, escrever);
            proximidade(alternativas, N_ALTERNATIVAS, proximidade, escrever);
            alternativaEscolhida(metodo, N_ALTERNATIVAS, escrever, alternativas, proximidade);
            escrever.format("%n%n%s", "Foi utilizado o metodo TOPSIS.");
            System.out.printf("%n%n%s", "Foi utilizado o metodo TOPSIS.");
        }
        escrever.close();   //fecha o formatter
    }

    /**
     * mostra o vetor dos pesos do método TOPSIS
     *
     * @param criterios guarda os critérios
     * @param N_CRITERIOS define o nº de critérios
     * @param pesos guarda os pesos dos critérios
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void vetorPesos(String[] criterios, int N_CRITERIOS, double[] pesos, Formatter escrever) {
        int i;
        System.out.printf("%s%n%n", "Vetor pesos:");
        escrever.format("%s%n%n", "Vetor pesos:");
        for (i = 0; i < N_CRITERIOS; i++) {
            System.out.printf("%s%4s", criterios[i], "");
            escrever.format("%s%4s", criterios[i], "");
        }
        System.out.printf("%n");
        escrever.format("%n");
        for (i = 0; i < N_CRITERIOS; i++) {
            System.out.printf("%.2f%4s", pesos[i], "");
            escrever.format("%.2f%4s", pesos[i], "");
        }
    }

    /**
     * mostra a matriz de critérios/alternativa do método TOPSIS
     *
     * @param alternativas guarda as alternativas
     * @param criterios guarda os critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param N_CRITERIOS define o nº de critérios
     * @param matriz guarda a matriz de critérios/alternativas
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void matrizDecisao(String[] alternativas, String[] criterios, int N_ALTERNATIVAS, int N_CRITERIOS, double[][] matriz, Formatter escrever) {
        int i, j;
        System.out.printf("%n%n%s%n%n", "Matriz decisao:");
        escrever.format("%n%n%s%n%n", "Matriz decisao:");
        for (i = 0; i < N_CRITERIOS; i++) {
            System.out.printf("%s%4s", criterios[i], "");
            escrever.format("%s%4s", criterios[i], "");
        }
        System.out.printf("%n");
        escrever.format("%n");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            System.out.printf("%s%4s", alternativas[i], "");
            escrever.format("%s%4s", alternativas[i], "");
            for (j = 0; j < N_CRITERIOS; j++) {
                System.out.printf("%.2f%4s", matriz[i][j], "");
                escrever.format("%.2f%4s", matriz[i][j], "");
            }
            System.out.printf("%n");
            escrever.format("%n");
        }
    }

    /**
     * mostra a matriz de critérios/alternativa normalizada do método TOPSIS
     *
     * @param alternativas guarda as alternativas
     * @param criterios guarda os critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param N_CRITERIOS define o nº de critérios
     * @param normalizada guarda a matriz critérios/alternativas normalizada
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void matrizNormalizada(String[] alternativas, String[] criterios, int N_ALTERNATIVAS, int N_CRITERIOS, double[][] normalizada, Formatter escrever) {
        int i, j;
        escrever.format("%n%s%n%n", "Matriz decisao normalizada:");
        for (i = 0; i < N_CRITERIOS; i++) {
            escrever.format("%s%4s", criterios[i], "");
        }
        escrever.format("%n");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            escrever.format("%s%4s", alternativas[i], "");
            for (j = 0; j < N_CRITERIOS; j++) {
                escrever.format("%.2f%4s", normalizada[i][j], "");
            }
            escrever.format("%n");
        }
    }

    /**
     * mostra a matriz de critérios/alternativas normalizada pesada do método
     * TOPSIS
     *
     * @param alternativas guarda as alternativas
     * @param criterios guarda os critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param N_CRITERIOS define o nº de critérios
     * @param pesada guarda a matriz de critérios/alternativas normalizada
     * pesada
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void matrizPesada(String[] alternativas, String[] criterios, int N_ALTERNATIVAS, int N_CRITERIOS, double[][] pesada, Formatter escrever) {
        int i, j;
        System.out.printf("%n%s%n%n", "Matriz decisao normalizada pesada:");
        escrever.format("%n%s%n%n", "Matriz decisao normalizada pesada:");
        for (i = 0; i < N_CRITERIOS; i++) {
            System.out.printf("%s%4s", criterios[i], "");
            escrever.format("%s%4s", criterios[i], "");
        }
        System.out.printf("%n");
        escrever.format("%n");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            System.out.printf("%s%4s", alternativas[i], "");
            escrever.format("%s%4s", alternativas[i], "");
            for (j = 0; j < N_CRITERIOS; j++) {
                System.out.printf("%.2f%4s", pesada[i][j], "");
                escrever.format("%.2f%4s", pesada[i][j], "");
            }
            System.out.printf("%n");
            escrever.format("%n");
        }
    }

    /**
     * mostra a solução ideal do método TOPSIS
     *
     * @param criterios guarda os critérios
     * @param N_CRITERIOS define o nº de critérios
     * @param solideal guarda a solução ideal
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void solucaoIdeal(String[] criterios, int N_CRITERIOS, double[] solideal, Formatter escrever) {
        int i;
        escrever.format("%n%s%n%n", "Solucao ideal:");
        for (i = 0; i < N_CRITERIOS; i++) {
            escrever.format("%s%4s", criterios[i], "");
        }
        escrever.format("%n");
        for (i = 0; i < N_CRITERIOS; i++) {
            escrever.format("%.2f%4s", solideal[i], "");
        }
    }

    /**
     * mostra a solução ideal negativa do método TOPSIS
     *
     * @param criterios guarda os critérios
     * @param N_CRITERIOS define o nº de critérios
     * @param solneg guarda a solução ideal negativa
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void solucaoNegativa(String[] criterios, int N_CRITERIOS, double[] solneg, Formatter escrever) {
        int i;
        escrever.format("%n%n%s%n%n", "Solucao ideal negativa:");
        for (i = 0; i < N_CRITERIOS; i++) {
            escrever.format("%s%4s", criterios[i], "");
        }
        escrever.format("%n");
        for (i = 0; i < N_CRITERIOS; i++) {
            escrever.format("%.2f%4s", solneg[i], "");
        }
    }

    /**
     * mostra as distâncias ideal e ideal negativa, para cada alternativa, no
     * método TOPSIS
     *
     * @param alternativas guarda as alternativas
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param disideal guarda a distância à solução ideal
     * @param disneg guarda a distância à solução ideal negativa
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void distancias(String[] alternativas, int N_ALTERNATIVAS, double[] disideal, double[] disneg, Formatter escrever) {
        int i;
        escrever.format("%n%n%s%n%n", "Distancia a solucao ideal:");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            escrever.format("%s%4s", alternativas[i], "");
            escrever.format("%.2f%n", disideal[i]);
        }
        escrever.format("%n%s%n%n", "Distancia a solucao ideal negativa:");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            escrever.format("%s%4s", alternativas[i], "");
            escrever.format("%.2f%n", disneg[i]);
        }
    }

    /**
     * mostra a proximidade relativa à solução ideal, para cada alternativa, no
     * método TOPSIS
     *
     * @param alternativas guarda as alternativas
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param proximidade guarda o vetor proximidade relativa à solução ideal
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     */
    public static void proximidade(String[] alternativas, int N_ALTERNATIVAS, double[] proximidade, Formatter escrever) {
        int i;
        System.out.printf("%n%s%n%n", "Proximidade relativa a solucao ideal:");
        escrever.format("%n%s%n%n", "Proximidade relativa a solucao ideal:");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            System.out.printf("%s%4s", alternativas[i], "");
            escrever.format("%s%4s", alternativas[i], "");
            System.out.printf("%.2f%n", proximidade[i]);
            escrever.format("%.2f%n", proximidade[i]);
        }
    }

    /**
     * mostra as matrizes de entrada do método AHP
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     * @param cabcomp guarda o cabeçalho das matrizes de comparação
     * @param copiacrit guarda a matriz dos critérios
     * @param copiacomp guarda as matrizes de comparação
     */
    public static void matrizesEntrada(int N_ALTERNATIVAS, Formatter escrever, String[][] cabcomp, double[][] copiacrit, double[][][] copiacomp) {
        int i, j, k;
        String[] aux;
        escrever.format("%s%n%n", "Matriz de entrada criterios:");
        System.out.printf("%s%n%n", "Matriz de entrada criterios:");    //imprime também na consola
        for (i = 0; i < copiacrit.length; i++) { //começa o ciclo dos cabeçalhos na posição 1 pois o conteúdo da posição 0 é irrelevante ("mc...")
            aux = cabcomp[i][0].split("_");
            if (copiacrit[i][0] != 0) {
                escrever.format("%s%4s", aux[1], "");
                System.out.printf("%s%4s", aux[1], "");
            }
        }
        escrever.format("%n");
        System.out.printf("%n");
        for (i = 0; i < copiacrit.length; i++) {
            aux = cabcomp[i][0].split("_");
            if (copiacrit[i][0] != 0) {
                escrever.format("%s%4s", aux[1], "");
                System.out.printf("%s%4s", aux[1], "");
                for (j = 0; j < copiacrit.length; j++) {
                    if (copiacrit[0][j] != 0) {
                        escrever.format("%.2f%4s", copiacrit[i][j], "");
                        System.out.printf("%.2f%4s", copiacrit[i][j], "");
                    }
                }
                escrever.format("%n");
                System.out.printf("%n");
            }
        }
        for (i = 0; i < copiacrit.length; i++) { //mesmo processo para as matrizes das alternativas
            aux = cabcomp[i][0].split("_");
            if (copiacrit[i][0] != 0) {
                escrever.format("%n%s%n%n", "Matriz de entrada comparacao " + aux[1] + ":");
                System.out.printf("%n%s%n%n", "Matriz de entrada comparacao " + aux[1] + ":");
                for (j = 1; j < N_ALTERNATIVAS + 1; j++) {
                    escrever.format("%s%4s", cabcomp[i][j], "");
                    System.out.printf("%s%4s", cabcomp[i][j], "");
                }
                for (j = 0; j < N_ALTERNATIVAS; j++) {
                    escrever.format("%n");
                    System.out.printf("%n");
                    escrever.format("%s%4s", cabcomp[0][j + 1], "");
                    System.out.printf("%s%4s", cabcomp[0][j + 1], "");
                    for (k = 0; k < N_ALTERNATIVAS; k++) {
                        escrever.format("%.2f%4s", copiacomp[i][j][k], "");
                        System.out.printf("%.2f%4s", copiacomp[i][j][k], "");
                    }
                }
                escrever.format("%n");
                System.out.printf("%n");
            }
        }
        escrever.format("%n");
        System.out.printf("%n");
    }

    /**
     * mostra as matrizes normalizadas do método AHP
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     * @param cabcomp guarda o cabeçalho das matrizes de comparação
     * @param crit guarda a matriz dos critérios normalizada
     * @param comp guarda as matrizes de comparação normalizadas
     * @param vetcomp guarda os vetores próprios associados às matrizes de
     * comparação
     * @param vetcrit guarda o vetor próprio associado à matriz dos critérios
     */
    public static void matrizesNormalizadas(int N_ALTERNATIVAS, Formatter escrever, String[][] cabcomp, double[][] crit, double[][][] comp, double[][] vetcomp, double[] vetcrit) {
        int i, j, k;
        String[] aux;
        escrever.format("%s%n%n", "Matriz normalizada criterios:");
        for (i = 0; i < crit.length; i++) {
            aux = cabcomp[i][0].split("_");
            if (crit[i][0] != 0) {
                escrever.format("%s%4s", aux[1], "");
            }
        }
        escrever.format("%s%4s", "Prioridade relativa", "");
        escrever.format("%n");
        for (i = 0; i < crit.length; i++) {
            aux = cabcomp[i][0].split("_");
            if (crit[i][0] != 0) {
                escrever.format("%s%4s", aux[1], "");
                for (j = 0; j < crit.length; j++) {
                    if (crit[0][j] != 0) {
                        escrever.format("%.2f%4s", crit[i][j], "");
                    }
                }
                escrever.format("%.2f", vetcrit[i]);
                escrever.format("%n");
            }
        }
        for (i = 0; i < crit.length; i++) { //processo semelhante para as matrizes das alternativas
            aux = cabcomp[i][0].split("_");
            if (crit[i][0] != 0) {
                escrever.format("%n%s%n%n", "Matriz normalizada comparacao " + aux[1] + ":");
                for (j = 1; j < N_ALTERNATIVAS + 1; j++) {
                    escrever.format("%s%4s", cabcomp[i][j], "");
                }
                escrever.format("%s%4s", "Prioridade relativa", "");
                for (j = 0; j < N_ALTERNATIVAS; j++) {
                    escrever.format("%n");
                    escrever.format("%s%4s", cabcomp[0][j + 1], "");
                    for (k = 0; k < N_ALTERNATIVAS; k++) {
                        escrever.format("%.2f%4s", comp[i][j][k], "");
                    }
                    escrever.format("%.2f", vetcomp[i][j]);
                }
                escrever.format("%n");
            }
        }
        escrever.format("%n");
    }

    /**
     * mostra o máximo valor próprio, o índice e a razão de consistência para
     * cada matriz utilizada no método AHP
     *
     * @param crit guarda a matriz dos critérios normalizada
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     * @param cabcomp guarda os cabeçalhos das matrizes de comparação
     * @param razcomp guarda as razões de consistência para cada matriz de
     * comparação
     * @param valcomp guarda o maior valor próprio associado a cada matriz de
     * comparação
     * @param indcomp guarda o índice de consistência associado a cada matriz de
     * comparação
     * @param valcrit guarda o maior valor próprio da matriz dos critérios
     * @param indcrit guarda o índice de consistência da matriz dos critérios
     * @param razcrit guarda a razão de consistência da matriz dos critérios
     */
    public static void valores(double[][] crit, Formatter escrever, String[][] cabcomp, double[] razcomp, double[] valcomp, double[] indcomp, double valcrit, double indcrit, double razcrit) {
        int i;
        String[] aux;
        escrever.format("%s%.2f%n", "Max. valor proprio matriz criterios: ", valcrit);
        escrever.format("%s%.2f%n", "IC matriz criterios: ", indcrit);
        escrever.format("%s%.2f%n%n", "RC matriz criterios: ", razcrit);

        for (i = 0; i < crit.length; i++) { //processo semelhante para as matrizes das alternativas
            aux = cabcomp[i][0].split("_");
            if (crit[i][0] != 0) {
                escrever.format("%s%.2f", "Max. valor proprio matriz " + aux[1] + ": ", valcomp[i]);
                escrever.format("%n%s%.2f%n", "IC matriz " + aux[1] + ": ", indcomp[i]);
                escrever.format("%s%.2f%n%n", "RC matriz " + aux[1] + ": ", razcomp[i]);
            }
        }
    }

    /**
     * mostra o vetor de prioridade composta do método AHP
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     * @param cabcomp guarda o cabeçalho das matrizes de comparação
     * @param vetalt guarda o vetor de prioridade composta
     */
    public static void prioridadeComposta(int N_ALTERNATIVAS, Formatter escrever, String[][] cabcomp, double[] vetalt) {
        int i;
        escrever.format("%s%n%n", "Vetor de prioridade composta:");
        System.out.printf("%s%n%n", "Vetor de prioridade composta:");
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            escrever.format("%s%4s", cabcomp[0][i + 1], "");    //cabcomp[0][i+1] define o nome da alternativa
            System.out.printf("%s%4s", cabcomp[0][i + 1], "");
            escrever.format("%.2f%n", vetalt[i]);
            System.out.printf("%.2f%n", vetalt[i]);
        }
    }

    /**
     * mostra os critérios excluidos no método AHP
     *
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * @param cabcomp guarda o cabeçalho das matrizes de comparação
     * @param crit guarda a matriz dos critérios normalizada
     */
    public static void criteriosExcluidos(Formatter escrever, String[][] cabcomp, double[][] crit) {
        int i;
        String[] aux;
        escrever.format("%n%n%s%n", "Criterios excluidos:");
        System.out.printf("%n%n%s%n", "Criterios excluidos:");
        for (i = 0; i < cabcomp.length; i++) {
            if (crit[i][0] == 0) {
                aux = cabcomp[i][0].split("_");
                escrever.format("%s%n", aux[1]);
                System.out.printf("%s%n", aux[1]);
            }
        }
    }

    /**
     * escolhe a melhor alternativa, consoante o método utilizado
     *
     * @param metodo define o método utilizado nos cálculos
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param escrever guarda o Formatter responsável pela escrita no ficheiro
     * de saída
     * @param cabcomp guarda as alternativas
     * @param vetalt guarda o vetor para escolha da melhor alternativa
     */
    public static void alternativaEscolhida(int metodo, int N_ALTERNATIVAS, Formatter escrever, String[] cabcomp, double[] vetalt) {
        int i, pos = 0; //pos define a posição do maior elemento no vetor
        double maior = 0;   //maior define o maior elemento no vetor
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            if (vetalt[i] > maior) {    //se o atual elemento no vetor de prioridade composta for superior ao conteúdo da variável maior
                maior = vetalt[i];  //atribui o atual elemento à variável maior
                if (metodo == 1) {
                    pos = i + 1;    //e a sua posição à variável pos
                }
                if (metodo == 2) {
                    pos = i;
                }
            }
        }
        escrever.format("%n%s", "A melhor alternativa e " + cabcomp[pos] + "."); //imprime a melhor alternativa
        System.out.printf("%n%s", "A melhor alternativa e " + cabcomp[pos] + ".");
    }

}
