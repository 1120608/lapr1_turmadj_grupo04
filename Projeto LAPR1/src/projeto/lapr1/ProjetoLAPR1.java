/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.lapr1;

import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 *
 * @author Grupo 4
 */
public class ProjetoLAPR1 {

    /**
     * @param args the command line arguments
     */
    private static final double[] valtab = {0, 0, 0.58, 0.9, 1.12, 1.24, 1.32, 1.41, 1.45, 1.49, 1.51, 1.48, 1.56, 1.57, 1.59}; //valores tabelados para cálculo da RC

    /**
     * função principal do projeto
     *
     * @param args parâmetros da linha de comandos
     * @throws FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        int N_CRITERIOS;    //guarda o nº de critérios
        int N_ALTERNATIVAS; //guarda o nº de alternativas
        String ficheiroErros = ""; //Váriavel que vai conter o nome do ficheiro de erros, caso seja necessário
        int i, j, k, metodo = 0;  //metodo indica o método a utilizar na resolução
        double limpeso = 0, limconsis = 0.1;  //limpeso indica o limiar para descartar pesos irrelevantes, enquanto que limconsis é o limiar de consistência da matriz
        int[] util = {-1, -1, 0, 0}; //permite verificar se o ficheiro contém o nº mínimo de matrizes necessárias
        boolean verif = true;   //variável que verifica a consistência das matrizes
        String op;  //variável que armazena a opção do utilizador
        if (args.length > 3) {  //se foram introduzidos pelo menos 4 argumentos
            String fich_in = args[args.length - 2];   //recebe o penúltimo argumento, define o nome do ficheiro de entrada
            String fich_out = args[args.length - 1];  //recebe o último argumento, define o nome do ficheiro de saída
            for (i = 0; i < args.length - 3; i++) { //analisa os parâmetros
                if (args[i].compareToIgnoreCase("-M") == 0) {   //se for -M
                    metodo = Integer.parseInt(args[i + 1]); //o parâmetro seguinte indica o método a utilizar
                }
                if (args[i].compareToIgnoreCase("-L") == 0) {   //se for -L
                    limpeso = Double.parseDouble(args[i + 1]);  //o parâmetro seguinte indica o limiar dos pesos
                }
                if (args[i].compareToIgnoreCase("-S") == 0) {   //se for -S
                    limconsis = Double.parseDouble(args[i + 1]);    //o parâmetro seguinte indica o limiar de consistência
                }
            }
            if (metodo == 1) {  //se o método a utilizar é o AHP (-M 1 na linha de comandos)
                util = LeituraEscrita.auxLerFicheiro(fich_in, metodo, util);   //lê o nº de critérios e alternativas, numa primeira fase, para definição de variáveis

                N_CRITERIOS = util[2];  //define o nº de critérios
                N_ALTERNATIVAS = util[3];   //define o nº de alternativas

                double[][] crit = new double[N_CRITERIOS][N_CRITERIOS];    //matriz critérios normalizada
                double[][] copiacrit = new double[N_CRITERIOS][N_CRITERIOS];   //matriz critérios
                double[][][] comp = new double[N_CRITERIOS][N_ALTERNATIVAS][N_ALTERNATIVAS];   //matrizes alternativas normalizadas
                double[][][] copiacomp = new double[N_CRITERIOS][N_ALTERNATIVAS][N_ALTERNATIVAS];  //matrizes alternativas
                double[][] vetcomp = new double[N_CRITERIOS][N_ALTERNATIVAS];  //vetores próprios associados às matrizes de alternativas
                double[] vetcrit = new double[N_CRITERIOS];    //vetor próprio associados à matriz dos critérios
                double[] valcomp = new double[N_CRITERIOS];    //máximo valor próprio associado às matrizes de alternativas
                double[] indcomp = new double[N_CRITERIOS];    //IC associado às matrizes de alternativas
                String[][] cabcomp = new String[N_CRITERIOS][N_ALTERNATIVAS + 1];  //cabeçalho das matrizes de alternativas
                double[] vetalt = new double[N_ALTERNATIVAS];  //vetor de prioridade composta
                double[] razcomp = new double[N_CRITERIOS];    //RC associada às matrizes de alternativas
                double valcrit = 0;  //máximo valor próprio associado à matriz dos critérios
                double indcrit = 0;  //IC associado à matriz dos critérios
                double razcrit = 0;  //RC associada à matriz dos critérios
                String[] beneficio = null;   //vetor com os critérios de benefício (não necessário para o método AHP)
                String[] custo = null;   //vetor com os critérios de custo (não necessário para o método AHP)
                double[] pesos = null;   //vetor com os pesos dos critérios (não necessário para o método AHP)
                String[] criterios = null;   //vetor com todos os critérios (não necessário para o método AHP)
                String[] alternativas = null;    //vetor com as alternativas (não necessário para o método AHP)
                double[][] matriz = null;   //matriz critério-alternativa (não necessário para o método AHP)
                double[][] normalizada = null;   //matriz critério-alternativa normalizada (não necessário para o método AHP)
                double[][] pesada = null;   //matriz critério-alternativa normalizada e pesada (não necessário para o método AHP)
                double[] solideal = null;  //vetor com a solução ideal (não necessário para o método AHP)
                double[] solneg = null;  //vetor com a solução ideal negativa (não necessário para o método AHP)
                double[] disideal = null;  //vetor com a distância de cada alternativa à solução ideal (não necessário para o método AHP)
                double[] disneg = null;  //vetor com a distância de cada alternativa à solução ideal negativa (não necessário para o método AHP)
                double[] proximidade = null;  //vetor com a proximidade relativa à solução ideal para cada alternativa (não necessário para o método AHP)

                util = LeituraEscrita.lerFicheiro(ficheiroErros, N_CRITERIOS, N_ALTERNATIVAS, fich_in, metodo, util, beneficio, custo, pesos, criterios, alternativas, matriz, cabcomp, crit, comp);   //lê o nº de critérios e alternativas, numa primeira fase, para definição de variáveis
                if (util[0] < 3) {  //se tem menos de 2 matrizes no caso de usarmos o método AHP
                    String erro = "O ficheiro nao contem o nº minimo de matrizes necessario.";          //Mensagem de erro
                    System.out.print(erro);  //apresenta mensagem de erro relevante
                    ficheiroErros = LogErros.guardaErros(erro, ficheiroErros);                          //envia a mensagem de erro para ser gravada num ficheiro
                } else {    //senão, continua o processamento
                    AHP.copiarMatrizes(N_ALTERNATIVAS, crit, copiacrit, comp, copiacomp);
                    AHP.normalizarMatrizes(N_ALTERNATIVAS, crit, comp);   //normaliza as matrizes
                    Scanner ler = new Scanner(System.in);   //define o input do scanner como o teclado
                    System.out.println("Quer os calculos com valores exatos(E) ou aproximados(A)? (por defeito exatos)");
                    op = ler.next();    //lê a opção do utilizador
                    if (op.toUpperCase().charAt(0) == 'A') {    //se a opção do utilizador contém a/A (ignore case) na posição 0 da string
                        AHP.vetorProprio(N_CRITERIOS, N_ALTERNATIVAS, crit, comp, vetcomp, vetcrit); //faz o cálculo aproximado dos vetores próprios 
                        valcrit = AHP.valorProprio(N_CRITERIOS, N_ALTERNATIVAS, copiacrit, copiacomp, vetcomp, vetcrit, valcomp, valcrit); //faz o cálculo aproximado dos valores próprios 
                    } else {
                        valcrit = AHP.valoresExatos(copiacrit, copiacomp, vetcomp, vetcrit, valcomp, valcrit);    //faz o cálculo exato dos valores e vetores próprios
                    }
                    N_CRITERIOS = AHP.limparMatrizes(limpeso, N_CRITERIOS, N_ALTERNATIVAS, vetcrit, copiacrit, copiacomp);
                    AHP.copiarMatrizes(N_ALTERNATIVAS, copiacrit, crit, copiacomp, comp);
                    AHP.normalizarMatrizes(N_ALTERNATIVAS, crit, comp);   //normaliza as matrizes
                    if (op.toUpperCase().charAt(0) == 'A') {    //se a opção do utilizador contém a/A (ignore case) na posição 0 da string
                        AHP.vetorProprio(N_CRITERIOS, N_ALTERNATIVAS, crit, comp, vetcomp, vetcrit); //faz o cálculo aproximado dos vetores próprios 
                        valcrit = AHP.valorProprio(N_CRITERIOS, N_ALTERNATIVAS, copiacrit, copiacomp, vetcomp, vetcrit, valcomp, valcrit); //faz o cálculo aproximado dos valores próprios 
                    } else {
                        valcrit = AHP.valoresExatos(copiacrit, copiacomp, vetcomp, vetcrit, valcomp, valcrit);    //faz o cálculo exato dos valores e vetores próprios
                    }
                    indcrit = AHP.indiceConsistencia(N_CRITERIOS, N_ALTERNATIVAS, valcomp, indcomp, valcrit, indcrit);   //calcula os ICs
                    razcrit = AHP.razaoConsistencia(valtab, N_CRITERIOS, N_ALTERNATIVAS, razcomp, indcomp, indcrit, razcrit);   //calcula as RCs
                    for (i = 0; i < N_CRITERIOS; i++) {
                        if (razcomp[i] > limconsis) { //se a RC for superior ao limiar de consistência
                            verif = false;  //as matrizes não são consistentes
                        }
                    }
                    if (razcrit > limconsis) {
                        verif = false;
                    }
                    if (verif) {    //se as matrizes são consistentes
                        AHP.vetorAlternativa(N_ALTERNATIVAS, vetalt, vetcomp, vetcrit); //calcula o vetor de prioridade composta
                        LeituraEscrita.outputs(proximidade, disneg, disideal, solneg, solideal, pesada, normalizada, matriz, alternativas, criterios, pesos, metodo, N_CRITERIOS, N_ALTERNATIVAS, fich_out, cabcomp, vetalt, razcomp, crit, copiacrit, comp, copiacomp, vetcomp, vetcrit, valcomp, indcomp, valcrit, indcrit, razcrit);  //imprime os outputs necessários
                    } else {    //senão forem consistentes
                        String erro = "As matrizes nao sao consistentes.";          //Mensagem de erro
                        System.out.print(erro);  //apresenta mensagem de erro relevante
                        ficheiroErros = LogErros.guardaErros(erro, ficheiroErros);              //envia a mensagem de erro para ser gravada num ficheiro
                    }
                }
            }
            if (metodo == 2) {  //se o método a utilizar é o TOPSIS (-M 2 na linha de comandos)
                util = LeituraEscrita.auxLerFicheiro(fich_in, metodo, util);   //lê os dados do ficheiro

                N_CRITERIOS = util[2];  //define o nº de critérios
                N_ALTERNATIVAS = util[3];   //define o nº de alternativas

                String[] beneficio = new String[N_CRITERIOS];  //vetor com os critérios de benefício
                String[] custo = new String[N_CRITERIOS];  //vetor com os critérios de custo
                double[] pesos = new double[N_CRITERIOS];  //vetor com os pesos dos critérios
                String[] criterios = new String[N_CRITERIOS];  //vetor com todos os critérios
                String[] alternativas = new String[N_ALTERNATIVAS];    //vetor com as alternativas
                double[][] matriz = new double[N_ALTERNATIVAS][N_CRITERIOS];   //matriz critério-alternativa
                double[][] normalizada = new double[N_ALTERNATIVAS][N_CRITERIOS];   //matriz critério-alternativa normalizada
                double[][] pesada = new double[N_ALTERNATIVAS][N_CRITERIOS];   //matriz critério-alternativa normalizada e pesada
                double[] solideal = new double[N_CRITERIOS];  //vetor com a solução ideal
                double[] solneg = new double[N_CRITERIOS];  //vetor com a solução ideal negativa
                double[] disideal = new double[N_ALTERNATIVAS];  //vetor com a distância de cada alternativa à solução ideal
                double[] disneg = new double[N_ALTERNATIVAS];  //vetor com a distância de cada alternativa à solução ideal negativa
                double[] proximidade = new double[N_ALTERNATIVAS];  //vetor com a proximidade relativa à solução ideal para cada alternativa
                String[][] cabcomp = null;  //cabeçalho das matrizes de alternativas (não necessária para o TOPSIS)
                double[][] crit = null;    //matriz critérios normalizada (não necessária para o TOPSIS)
                double[][][] comp = null;   //matrizes alternativas normalizadas (não necessária para o TOPSIS)
                double[] vetalt = null;  //vetor de prioridade composta (não necessária para o TOPSIS)
                double[] razcomp = null;    //RC associada às matrizes de alternativas (não necessária para o TOPSIS)
                double[][] copiacrit = null;   //matriz critérios (não necessária para o TOPSIS)
                double[][][] copiacomp = null;  //matrizes alternativas (não necessária para o TOPSIS)
                double[][] vetcomp = null;  //vetores próprios associados às matrizes de alternativas (não necessária para o TOPSIS)
                double[] vetcrit = null;    //vetor próprio associados à matriz dos critérios (não necessária para o TOPSIS)
                double[] valcomp = null;    //máximo valor próprio associado às matrizes de alternativas (não necessária para o TOPSIS)
                double[] indcomp = null;    //IC associado às matrizes de alternativas (não necessária para o TOPSIS)
                double valcrit = 0;  //máximo valor próprio associado à matriz dos critérios (não necessária para o TOPSIS)
                double indcrit = 0;  //IC associado à matriz dos critérios (não necessária para o TOPSIS)
                double razcrit = 0;  //RC associada à matriz dos critérios (não necessária para o TOPSIS)

                util = LeituraEscrita.lerFicheiro(ficheiroErros, N_CRITERIOS, N_ALTERNATIVAS, fich_in, metodo, util, beneficio, custo, pesos, criterios, alternativas, matriz, cabcomp, crit, comp);

                if (util[1] < 1) //se não for lida a matriz
                {
                    String erro = "Nao foi encontrada a matriz.";           //Mensagem de erro
                    System.out.print(erro);   //imprime mensagem de erro relevante
                    ficheiroErros = LogErros.guardaErros(erro, ficheiroErros);              //envia a mensagem de erro para ser gravada num ficheiro
                } else //senão, continua o processamento
                {
                    TOPSIS.normalizarTopsis(N_CRITERIOS, N_ALTERNATIVAS, matriz, normalizada); //normaliza a matriz
                    TOPSIS.pesarTopsis(N_CRITERIOS, N_ALTERNATIVAS, pesos, normalizada, pesada);  //constrói a matriz normalizada pesada
                    TOPSIS.solucoesIdeais(N_CRITERIOS, N_ALTERNATIVAS, beneficio, custo, criterios, pesada, solideal, solneg);   //obtém as soluções ideais
                    TOPSIS.distanciasIdeais(N_CRITERIOS, N_ALTERNATIVAS, pesada, solideal, solneg, disideal, disneg); //obtém as distâncias às soluções ideais
                    TOPSIS.proximidadeRelativa(N_ALTERNATIVAS, disideal, disneg, proximidade);  //calcula a proximidade relativa à solução ideal, para cada alternativa
                    LeituraEscrita.outputs(proximidade, disneg, disideal, solneg, solideal, pesada, normalizada, matriz, alternativas, criterios, pesos, metodo, N_CRITERIOS, N_ALTERNATIVAS, fich_out, cabcomp, vetalt, razcomp, crit, copiacrit, comp, copiacomp, vetcomp, vetcrit, valcomp, indcomp, valcrit, indcrit, razcrit);  //imprime os outputs necessários
                }
            }
        } else {    //senão forem introduzidos pelo menos 2 argumentos
            String erro = "Nao foi introduzido o nº de argumentos necessario."; //Mensagem de erro
            System.out.print(erro); //apresenta mensagem de erro relevante
            ficheiroErros = LogErros.guardaErros(erro, ficheiroErros);                      //envia a mensagem de erro para ser gravada num ficheiro
        }
        if (!ficheiroErros.equals("")) {      //Se a variavel "ficheiroErros" nao estiver vazia significa que um ficheiro de erros foi aberto
            LogErros.fechaErros();              //fecha o ficheiro de erros caso seja necessario
        }
    }
}
