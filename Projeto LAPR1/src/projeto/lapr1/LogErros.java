/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.lapr1;

import java.io.File;
import java.io.FileNotFoundException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;

/**
 *
 * @author Grupo 4
 */
public class LogErros {

    public static Formatter Format;

    /**
     * permite obter a data atual
     *
     * @return
     */
    public static String DataAgora() {
        Date agora = new Date();
        SimpleDateFormat ft = new SimpleDateFormat("yyyy-MM-dd'_'HH.mm.ss'.txt'");
        String data = ft.format(agora);
        return data;
    }

    /**
     * guarda os erros no ficheiro
     *
     * @param erro String que contém o erro a registar
     * @param nome nome do ficheiro para guardar os erros
     * @return
     * @throws FileNotFoundException
     */
    public static String guardaErros(String erro, String nome) throws FileNotFoundException {
        if (nome.equals("")) {
            nome = DataAgora();
            Format = new Formatter(new File(nome));
            Format.format("%s%n", erro);
        } else {
            Format.format("%s%n", erro);
        }
        return nome;
    }

    /**
     * fecha a escrita no ficheiro de erros
     */
    public static void fechaErros() {
        Format.close();
    }
}
