/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package projeto.lapr1;

import java.io.FileNotFoundException;

/**
 *
 * @author Grupo 4
 */
public class TOPSIS {

    /**
     * guarda as matrizes em memória
     *
     * @param ficheiroErros guarda o nome do ficheiro de erros
     * @param N_CRITERIOS define o nº de critérios
     * @param linha guarda o contéudo da linha atual não-vazia
     * @param util guarda o estado atual da leitura do ficheiro
     * @param beneficio guarda os critérios beneficio
     * @param custo guarda os critérios custo
     * @param pesos guarda os pesos dos critérios
     * @param criterios guarda todos os critérios
     * @param alternativas guarda as alternativas
     * @param matriz guarda a matriz critérios/alternativas
     * @return
     * @throws FileNotFoundException
     */
    public static int[] guardarTopsis(String ficheiroErros, int N_CRITERIOS, String linha, int[] util, String[] beneficio, String[] custo, double[] pesos, String[] criterios, String[] alternativas, double[][] matriz) throws FileNotFoundException {

        int i;
        String temp = linha.replaceAll(" +", " ");  //apaga os espaços múltiplos da variável linha e atribui o resultado à variável temp
        String[] valor = temp.split(" ");   //separa o conteúdo da variável temp com auxílio do delimitador " " e atribui o resultado ao vetor de strings valor
        if (valor[0].compareToIgnoreCase("crt_beneficio") == 0) {   //se a linha contém os critérios definidos como benefício
            for (i = 1; i < valor.length; i++) {
                beneficio[i - 1] = valor[i];    //preenche o vetor beneficio com os critérios definidos como benefício
            }
            util[0] = -1; //definir valores adequados ao método na variável de controlo util
            util[1] = -1; //definir valores adequados ao método na variável de controlo util
        }
        if (valor[0].compareToIgnoreCase("crt_custo") == 0) {   //se a linha contém os critérios definidos como custo
            for (i = 1; i < valor.length; i++) {
                custo[i - 1] = valor[i];    //preenche o vetor custo com os critérios definidos como custo
            }
        }
        if (util[0] == 0) { //se estão a ser lidos os pesos
            util[0] = -1;    //reset a verif
            for (i = 0; i < valor.length; i++) {
                pesos[i] = Double.parseDouble(valor[i]);  //preenche o vetor pesos com os pesos dos critérios
            }
        }
        if (valor[0].compareToIgnoreCase("vec_pesos") == 0) {   //se estivermos na presença numa linha de critérios começada por "vec_pesos"
            util[0] = 0;   //na linha seguinte irá conter os respetivos pesos
        }

        if (valor[0].compareToIgnoreCase("crt") == 0) { //se a linha contém os critérios
            for (i = 1; i < valor.length; i++) {
                criterios[i - 1] = valor[i];  //preenche o vetor critérios com os critérios
            }
        }
        if (util[1] > -1) //se a matriz está a ser lida
        {
            if (beneficio[0] == null && custo[0] == null) { //se não foram encontrados os critérios benefícios e custos
                for (i = 0; i < N_CRITERIOS; i++) {
                    beneficio[i] = criterios[i];
                }
            }
            if (pesos[0] == 0) {    //se não foi lido o vetor pesos
                String erro = "O ficheiro nao contem o nº minimo de matrizes necessario.";          //Mensagem de erro
                System.out.printf("%s%n%n", erro);  //apresenta mensagem de erro relevante
                ficheiroErros = LogErros.guardaErros(erro, ficheiroErros);
                System.out.printf("%s%n%n", "ATENCAO: OS PESOS NAO FORAM DEFINIDOS NO FICHEIRO DE ENTRADA!");   //apresenta mensagem de erro relevante
                for (i = 0; i < N_CRITERIOS; i++) {
                    pesos[i] = 1.0 / N_CRITERIOS;   //distribui os pesos equititivamente pelos critérios
                }
            }
            for (i = 0; i < valor.length; i++) {
                matriz[util[1]][i] = Double.parseDouble(valor[i]);  // preenche a matriz
            }
            util[1]++;  //incrementa a linha atual da matriz
        }
        if (valor[0].compareToIgnoreCase("alt") == 0) { //se a linha contém as alternativas
            for (i = 1; i < valor.length; i++) {
                alternativas[i - 1] = valor[i];  //preenche o vetor alternativas com as alternativas
            }
            util[1] = 0;  //indica o início da leitura da matriz
        }
        return util;    //retorna o estado atual da leitura do ficheiro
    }

    /**
     * normaliza a matriz critérios/alternativas
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param matriz guarda a matriz critérios/alternativas
     * @param normalizada guarda a matriz critérios/alternativas normalizada
     */
    public static void normalizarTopsis(int N_CRITERIOS, int N_ALTERNATIVAS, double[][] matriz, double[][] normalizada) {
        int i, j;
        double somac = 0, raiz = 0;   //armazena a soma dos quadrados dos elementos da coluna atual
        boolean flag = false; //permite saber se a normalização já foi feita na coluna atual
        for (i = 0; i < N_CRITERIOS; i++) {
            for (j = 0; j < N_ALTERNATIVAS; j++) {
                if (!flag) {    //se a normalização não foi feita
                    somac = somac + Math.pow(matriz[j][i], 2);  //fazer a soma dos quadrados do elementos da coluna atual
                } else {    //senão
                    normalizada[j][i] = matriz[j][i] / raiz;    //efetuar o quociente entre o elemento atual da matriz e a raiz quadrada de somac e atribuir à matriz normalizada
                }
            }
            flag = false;   //definir a normalização da coluna atual como feita
            if (somac != 0) {   //se a normalização não foi feita, é porque a variável somac tem conteúdo
                raiz = Math.sqrt(somac);    //faz-se a raiz quadrada de somac e atribui-se à variável raiz
                flag = true;    //definir a normalização como pronta a fazer
                somac = 0;  //reset à variável somac
                i--;    //diminuir a sentinela do ciclo das linhas, de modo a fazer a normalização da coluna na próxima iteração
            }
        }
    }

    /**
     * pesa a matriz critérios/alternativas normalizada
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param pesos guarda os pesos dos critérios
     * @param normalizada guarda a matriz critérios/alternativas normalizada
     * @param pesada guarda a matriz critérios/alternativas normalizada pesada
     */
    public static void pesarTopsis(int N_CRITERIOS, int N_ALTERNATIVAS, double[] pesos, double[][] normalizada, double[][] pesada) {
        int i, j;

        for (i = 0; i < N_ALTERNATIVAS; i++) {
            for (j = 0; j < N_CRITERIOS; j++) {
                pesada[i][j] = normalizada[i][j] * pesos[j];    //fazer o produto entre a matriz normalizada e o peso da coluna atual e atribuir à matriz pesada
            }
        }
    }

    /**
     * calcula as soluções ideais e ideais negativas
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param beneficio guarda os critérios beneficio
     * @param custo guarda os critérios custo
     * @param criterios guarda todos os critérios
     * @param pesada guarda a matriz critérios/alternativas normalizada pesada
     * @param solideal guarda a solução ideal
     * @param solneg guarda a solução ideal negativa
     */
    public static void solucoesIdeais(int N_CRITERIOS, int N_ALTERNATIVAS, String[] beneficio, String[] custo, String[] criterios, double[][] pesada, double[] solideal, double[] solneg) {
        int i, j, k;
        double maior = -1, menor = 10; //armazenam o maior e menor valor, respetivamente, da coluna atual
        for (i = 0; i < N_CRITERIOS; i++) {
            for (j = 0; j < N_ALTERNATIVAS; j++) {
                if (pesada[j][i] > maior) //se for o atual maior elemento da coluna
                {
                    maior = pesada[j][i]; //atribui o seu valor à variavel maior
                }
                if (pesada[j][i] < menor) //se for o atual menor elemento da coluna
                {
                    menor = pesada[j][i]; //atribui o seu valor à variavel menor
                }
            }
            for (k = 0; k < beneficio.length; k++) {
                if (beneficio[k] != null && beneficio[k].compareToIgnoreCase(criterios[i]) == 0) //se o critério atual é um critério benefício
                {
                    solideal[i] = maior;  //atribui o maior valor da coluna ao vetor solideal na posição atual
                    solneg[i] = menor;    //atribui o menor valor da coluna ao vetor solneg na posição atual
                }
                if (custo[k] != null && custo[k].compareToIgnoreCase(criterios[i]) == 0) //se o critério atual é um critério custo
                {
                    solideal[i] = menor;  //atribui o menor valor da coluna ao vetor solideal na posição atual
                    solneg[i] = maior;    //atribui o maior valor da coluna ao vetor solneg na posição atual
                }
            }
            maior = -1; //reset à variável maior
            menor = 10; //reset à variável menor
        }
    }

    /**
     * calcula as distâncias à solução ideal e ideal negativa
     *
     * @param N_CRITERIOS define o nº de critérios
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param pesada guarda a matriz critérios/alternativas normalizada pesada
     * @param solideal guarda a solução ideal
     * @param solneg guarda a solução ideal negativa
     * @param disideal guarda a distância à solução ideal
     * @param disneg guarda a distância à solução ideal negativa
     */
    public static void distanciasIdeais(int N_CRITERIOS, int N_ALTERNATIVAS, double[][] pesada, double[] solideal, double[] solneg, double[] disideal, double[] disneg) {
        int i, j;
        double tmpideal = 0, tmpneg = 0;    //permite saber a soma dos quadrados da diferença entre a solução ideal/solução ideal negativa e a alternativa atual
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            for (j = 0; j < N_CRITERIOS; j++) {
                tmpideal = tmpideal + Math.pow(solideal[j] - pesada[i][j], 2); //faz o cálculo relativamente à solução ideal
                tmpneg = tmpneg + Math.pow(solneg[j] - pesada[i][j], 2);   //faz o cálculo relativamente à solução ideal negativa
            }
            disideal[i] = Math.sqrt(tmpideal);    //faz a raiz quadrada de tmpideal (distância à solução ideal para a alternativa atual)
            disneg[i] = Math.sqrt(tmpneg);    //faz a raiz quadrada de tmpneg (distância à solução ideal negativa para a alternativa atual)
            tmpideal = 0; //reset a tmpideal
            tmpneg = 0;   //reset a tmpneg
        }
    }

    /**
     * calcula a proximidade relativa à solução ideal
     *
     * @param N_ALTERNATIVAS define o nº de alternativas
     * @param disideal guarda a distância à solução ideal
     * @param disneg guarda a distância à solução ideal negativa
     * @param proximidade guarda a proximidade relativa à solução ideal, para
     * cada alternativa
     */
    public static void proximidadeRelativa(int N_ALTERNATIVAS, double[] disideal, double[] disneg, double[] proximidade) {
        int i;
        double soma = 0;
        boolean flag = false;
        for (i = 0; i < N_ALTERNATIVAS; i++) {
            if (flag) {
                proximidade[i] = proximidade[i] / soma;
            }
            if (!flag) {
                proximidade[i] = disneg[i] / (disideal[i] + disneg[i]); //calcula a proximidade relativa à solução ideal segundo a fórmula
                soma = soma + proximidade[i];
                if (i == N_ALTERNATIVAS - 1) {
                    i = -1;
                    flag = true;
                }
            }
        }
    }
}
