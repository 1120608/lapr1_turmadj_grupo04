Vetor pesos:

Estilo    Confiabilidade    Consumo    Custo    
0,10    0,40    0,30    0,20    

Matriz decisao:

Estilo    Confiabilidade    Consumo    Custo    
Civic    7,00    9,00    9,00    8,00    
Saturn    8,00    7,00    8,00    7,00    
Ford    9,00    6,00    8,00    9,00    
Mazda    6,00    7,00    8,00    6,00    

Matriz decisao normalizada:

Estilo    Confiabilidade    Consumo    Custo    
Civic    0,46    0,61    0,54    0,53    
Saturn    0,53    0,48    0,48    0,46    
Ford    0,59    0,41    0,48    0,59    
Mazda    0,40    0,48    0,48    0,40    

Matriz decisao normalizada pesada:

Estilo    Confiabilidade    Consumo    Custo    
Civic    0,05    0,25    0,16    0,11    
Saturn    0,05    0,19    0,15    0,09    
Ford    0,06    0,16    0,15    0,12    
Mazda    0,04    0,19    0,15    0,08    

Solucao ideal:

Estilo    Confiabilidade    Consumo    Custo    
0,06    0,25    0,16    0,08    

Solucao ideal negativa:

Estilo    Confiabilidade    Consumo    Custo    
0,04    0,16    0,15    0,12    

Distancia a solucao ideal:

Civic    0,03
Saturn    0,06
Ford    0,09
Mazda    0,06

Distancia a solucao ideal negativa:

Civic    0,09
Saturn    0,04
Ford    0,02
Mazda    0,05

Proximidade relativa a solucao ideal:

Civic    0,42
Saturn    0,23
Ford    0,10
Mazda    0,25

A melhor alternativa e Civic.

Foi utilizado o metodo TOPSIS.